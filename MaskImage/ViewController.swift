//
//  ViewController.swift
//  MaskImage
//
//  Created by Vo Truong Thi on 12/7/18.
//  Copyright © 2018 Vo Truong Thi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewContent: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let maskView = UIImageView.init(image: #imageLiteral(resourceName: "whalea"))
        maskView.contentMode = .scaleAspectFit
        maskView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        viewContent.mask = maskView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
}

